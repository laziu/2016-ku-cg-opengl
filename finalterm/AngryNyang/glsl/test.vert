#version 400 core

uniform mat4 MVP;

layout(location = 0) in vec4 vPosition;

void main()
{
    gl_Position = MVP * vPosition;
}
