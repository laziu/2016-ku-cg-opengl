#pragma once
#include "Mesh.h"
#include "Texture.h"

class SkyboxMesh: public Mesh
{
	std::shared_ptr<Texture> texture;

public:
	SkyboxMesh(std::shared_ptr<Program>& program);
	
public:
	void Bind() override;
};

