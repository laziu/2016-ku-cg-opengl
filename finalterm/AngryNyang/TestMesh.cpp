#include "TestMesh.h"
#include "DataIO.h"
#include "Texture.h"
#include "OBJ.h"
#include <glm/gtc/matrix_transform.hpp>

using namespace std;
using namespace glm;

TestMesh::TestMesh(): Mesh() {
	// load obj
	if ( false ) {
		OBJ obj("resource/interior.obj");
		obj.Serialize("resource/interior");
	}

	OBJ obj("resource/interior", OBJ::type::bin);

	// create vao
	vao = shared_ptr<VAO>(new VAO(obj, 0, 1, 2));

	// create program
	program = shared_ptr<Program>(new Program(
		IO::ReadString("glsl/common.vert"), IO::ReadString("glsl/common.frag")
	));

	// create texture
	texture = shared_ptr<Texture>(new Texture(BMP("resource/interior_less.bmp")));
}

void TestMesh::Bind(void) {
	Mesh::Bind();
	glActiveTexture(GL_TEXTURE5);
	texture->Bind();
	glUniform1i(program->getUniformLoation("TexData"), 5);
}
