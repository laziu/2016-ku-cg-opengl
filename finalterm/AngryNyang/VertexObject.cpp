#include "VertexObject.h"

VAO::VAO(OBJ const& obj, GLuint vertexLayout, GLuint texCoordLayout, GLuint normalLayout): VAO(TRIANGLES) {
	Bind();
	if ( vertexLayout >= 0 )
		SetVertexAttribPointer(std::shared_ptr<VBO>(new VBO(obj.vertices )), vertexLayout  );
	if ( texCoordLayout >= 0 )
		SetVertexAttribPointer(std::shared_ptr<VBO>(new VBO(obj.texCoords)), texCoordLayout);
	if ( normalLayout >= 0 )
		SetVertexAttribPointer(std::shared_ptr<VBO>(new VBO(obj.normals  )), normalLayout  );
}

VAO::~VAO() {
	Bind();
	for (auto const& i : vboMap) 
		glDisableVertexAttribArray(i.first);
	if ( id )
		glDeleteVertexArrays(1, &id);
}

void VAO::SetVertexAttribPointer(std::shared_ptr<VBO> const& vbo, GLuint attribIndex) {
	Bind();
	glEnableVertexAttribArray(attribIndex);
	vbo->Bind();
	glVertexAttribPointer(attribIndex, vbo->getAttribSize(), GL_FLOAT, GL_FALSE, 0, 0);
	vboMap[attribIndex] = vbo;
	if ( elementCount > vbo->getAttribCount() )
		elementCount = vbo->getAttribCount();
}

void VAO::SetVertexAttribPointer(std::shared_ptr<IBO> const& ibo) {
	Bind();
	ibo->Bind();
	this->ibo = ibo;
	iboInitialized = true;
}
