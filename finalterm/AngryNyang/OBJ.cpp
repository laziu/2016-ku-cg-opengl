#pragma warning(disable:4996)
#include "OBJ.h"
#include <cstdio>
#include <cstring>
#include "DataIO.h"

using namespace std;

namespace
{
	char const* whitespace = " \t\n\r";

	bool nextFloat(float& var) {
		auto t = strtok(NULL, whitespace);
		if ( t != NULL ) {
			var = float(atof(t));
			return true;
		}
		else 
			return false;
	}
	
	bool nextInt(int& var) {
		auto t = strtok(NULL, whitespace);
		if ( t != NULL ) {
			var = atoi(t);
			return true;
		}
		else
			return false;
	}
}

void OBJ::Load(string filename) {
	if ( initialized ) 
		return;

	FILE* file = fopen(filename.c_str(), "r");
	if ( !file ) {
		fprintf(stderr, "-- Error: %s is not exists.\n", filename.c_str());
		return;
	}

	// parse obj file first
	vector<glm::vec4> vertex_data;
	vector<glm::vec3> texCoord_data;
	vector<glm::vec3> normal_data;
	vector<glm::ivec3> index_data;

	char line[2000]; // max line width
	char* token;

	while ( fgets(line, 2000, file) ) {
		token = strtok(line, whitespace);

		if ( token == nullptr || strcmp(token, "//") == 0 || token[0] == '#' ) // skip comments
			;
		else if ( strcmp(token, "v") == 0 ) { // vertex
			glm::vec4 v(0.0f, 0.0f, 0.0f, 1.0f);
			nextFloat(v.x);
			nextFloat(v.y);
			nextFloat(v.z);
			nextFloat(v.w);
			vertex_data.push_back(v);
		}
		else if ( strcmp(token, "vt") == 0 ) { // texCoord
			glm::vec3 v(0.0f, 0.0f, 0.0f);
			nextFloat(v.x);
			nextFloat(v.y);
			nextFloat(v.z);
			texCoord_data.push_back(v);
		}
		else if ( strcmp(token, "vn") == 0 ) { // normal
			glm::vec3 v(1.0f, 0.0f, 0.0f);
			nextFloat(v.x);
			nextFloat(v.y);
			nextFloat(v.z);
			normal_data.push_back(v);
		}
		else if ( strcmp(token, "f") == 0 ) { // face
			vector<glm::ivec3> data;
			while ( (token = strtok(NULL, whitespace)) != NULL ) {
				glm::ivec3 v(-1, -1, -1);

				v.x = atoi(token) - 1;

				if ( strstr(token, "//") != NULL ) // v,n
					v.z = atoi(strchr(token, '/') + 2) - 1;
				else if ( strstr(token, "/") != NULL ) {
					auto tt = strchr(token, '/') + 1;
					v.y = atoi(tt) - 1;
					if ( strstr(tt, "/") != NULL )
						v.z = atoi(strchr(tt, '/') + 1) - 1;
				}
				data.push_back(v);
			}
			for ( unsigned int i = 2; i < data.size(); ++i ) {
				index_data.push_back(data[0    ]);
				index_data.push_back(data[i - 1]);
				index_data.push_back(data[i    ]);
			}
		}
	}
	fclose(file);
	// generate vector to use in opengl properly
	for ( auto& i : index_data ) {
		vertices .push_back(i.x >= 0 ?   vertex_data[i.x] : glm::vec4(0, 0, 0, 0));
		texCoords.push_back(i.y >= 0 ? texCoord_data[i.y] : glm::vec3(0, 0, 0));
		normals  .push_back(i.z >= 0 ?   normal_data[i.z] : glm::vec3(0, 0, 0));
	}
	
	initialized = true;
}

void OBJ::Unload() {
	if ( !initialized )
		return;

	vertices .clear();
	texCoords.clear();
	normals  .clear();

	initialized = false;
}

void OBJ::Reload(std::string binname) {
	if ( initialized )
		return;
	
	vertices  = IO::ReadBinary<glm::vec4>(binname + ".vrt.bin");
	texCoords = IO::ReadBinary<glm::vec3>(binname + ".tex.bin");
	normals   = IO::ReadBinary<glm::vec3>(binname + ".nrm.bin");
	
	if ( vertices.empty() || texCoords.empty() || normals.empty() )
		return;

	initialized = true;
}

bool OBJ::Serialize(std::string binname) {
	return IO::WriteBinary(binname + ".vrt.bin", vertices )
		&& IO::WriteBinary(binname + ".tex.bin", texCoords)
		&& IO::WriteBinary(binname + ".nrm.bin", normals  );
}
