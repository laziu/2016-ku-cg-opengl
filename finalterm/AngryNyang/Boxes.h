#pragma once
#include <glm/glm.hpp>
#include <vector>
#include <array>
#include <string>
#include "macro.h"
class Boxes
{
public:
	struct Box 
	{ 
		std::array<glm::vec3, 8> point; 
	};

	struct Object
	{
		std::string name;
		std::vector<Box> box;
	};
private:
	bool initialized;
public:
	std::vector<Object> object;

	Boxes(): initialized(false) {}

	Boxes(std::string filename): Boxes() {
		Load(filename);
	}
	
	bool operator!() const {
		return !initialized;
	}

	FORCED_MOVE_CONSTRUCTOR(Boxes): Boxes() {
		std::swap(initialized, o.initialized);
		std::swap(object     , o.object     );
	}

public:
	void Load(std::string filename);
	void Unload();
};


Boxes::Box player_init();