#include "FireBallGenerator.h"
#include "DataIO.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/norm.hpp>
#include <cstdio>
#include <cstdlib>

using namespace std;
using namespace glm;

void FireBall::UpdateParticles(float dT, vec3 cameraPosition, vec3 cameraDirection) {
	cameraDirection = normalize(cameraDirection);

	// Add new particles for each frame
	if ( life ) {
		int newParticles = int(dT * 1000.0f);
		if ( newParticles > int(0.016f * 1000.0f) )
			newParticles = int(0.016f * 1000.0f);

		for ( int i = 1; i <= newParticles; ++i ) {
			Particle& p = particles.back();
			p.life = 2.0f;
			p.position = position;
		
			vec3 randdir(
				(rand() % 2000 - 1000.0f) / 1000.0f,
				(rand() % 2000 - 1000.0f) / 1000.0f,
				(rand() % 2000 - 1000.0f) / 1000.0f
			);

			p.velocity = randdir * spread;

			p.size = (rand() % 1000) / 500.0f + 0.1f;
		}
	}

	// Simulate all particles
	int particlesCount = 0;
	for ( auto& p : particles ) {
		p.life -= dT;
		if ( p.life > 0.0f ) {
			// float smoke
			p.velocity += vec3(0.0f, 0.5f, 0.0f) * dT * 0.5f;
			p.position += p.velocity * dT;
			p.cameradistance = length2(dot(p.position - cameraPosition, cameraDirection));
		}
		else {
			p.cameradistance = -1.0f;
		}
	}

	std::sort(particles.begin(), particles.end());
}


FireBallGenerator::FireBallGenerator() {
	// generate particle mesh
	particleVao = shared_ptr<VAO>(new VAO(VAO::TRIANGLE_STRIP));
	particleVao->Bind();

	particleProgram = shared_ptr<Program>(new Program(
		IO::ReadString("glsl/particle.vert"), IO::ReadString("glsl/particle.frag")
	));

	cameraRightId = particleProgram->getUniformLoation("CameraRight");
	cameraUpId    = particleProgram->getUniformLoation("CameraUp"   );
	vpId          = particleProgram->getUniformLoation("VP"         );

	particleTex = shared_ptr<Texture>(new Texture(TGA("resource/flame.tga")));
	
	vector<vec2> g_vertex_buffer_data = {
		vec2(-0.5f, -0.5f),
		vec2( 0.5f, -0.5f),
		vec2(-0.5f,  0.5f),
		vec2( 0.5f,  0.5f)
	};

	shared_ptr<VBO> billboard_vertex(new VBO(g_vertex_buffer_data));
	particlePosVbo = shared_ptr<VBO>(new VBO(FireBall::ContainerSize, sizeof(vec4)));
	particleAlphaVbo = shared_ptr<VBO>(new VBO(FireBall::ContainerSize, sizeof(GLfloat)));

	particleVao->SetVertexAttribPointer(billboard_vertex, 0);
	particleVao->SetVertexAttribPointer(particlePosVbo  , 1);
	particleVao->SetVertexAttribPointer(particleAlphaVbo, 2);

	// These functions are specific to glDrawArrays Instanced
	glVertexAttribDivisor(0, 0);  // reuse same vertices
	glVertexAttribDivisor(1, 1);  // one per quad
	glVertexAttribDivisor(2, 1);  // one per quad
}

void FireBallGenerator::Update(float dT, glm::vec3 cameraPosition, glm::vec3 cameraDirection) {
	for ( int i = 0; i < fireballs.size(); ++i )
		fireballs[i].UpdateParticles(dT, cameraPosition, cameraDirection);
}

void FireBallGenerator::Draw(glm::mat4 vMat, glm::mat4 pMat) {
	// draw particles
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	struct SimpleParticle
	{
		glm::vec4 xyzs;
		float life;
		float dist;
		SimpleParticle(vec3 p, float s, float l, float d):
			xyzs(vec4(p, s)), life(l), dist(d) {}
		bool operator<(SimpleParticle const& o) const {
			return this->dist > o.dist;
		}
	};

	vector<SimpleParticle> plist;

	for ( auto& ball : fireballs ) {
		for ( int i = 0; i < ball.particles.size(); ++i ) {
			Particle &p = ball.particles[i];
			if ( p.life > 0.0f )
				plist.push_back(SimpleParticle(p.position, p.size, p.life, p.cameradistance));
			else break;
		}
	}

	sort(plist.begin(), plist.end());

	for ( int i = 0; i < ContainerSize && i < plist.size(); ++i ) {
		particle_xyzs_buffer[i] = plist[i].xyzs;
		particle_alpha_buffer[i] = plist[i].life;
	}

	particleVao->Bind();
	particlePosVbo->Bind();
	glBufferData(GL_ARRAY_BUFFER, particle_xyzs_buffer.size() * sizeof(vec4), NULL, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, particle_xyzs_buffer.size() * sizeof(vec4), particle_xyzs_buffer.data());
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);
	particleAlphaVbo->Bind();
	glBufferData(GL_ARRAY_BUFFER, particle_alpha_buffer.size() * sizeof(GLfloat), NULL, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, particle_alpha_buffer.size() * sizeof(GLfloat), particle_alpha_buffer.data());
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, 0);

	particleProgram->Use();
	glActiveTexture(GL_TEXTURE2);
	particleTex->Bind();
	glUniform1i(particleProgram->getUniformLoation("TexData"), 2);

	glUniform3f(cameraRightId, vMat[0][0], vMat[1][0], vMat[2][0]);
	glUniform3f(cameraUpId, vMat[0][1], vMat[1][1], vMat[2][1]);
	mat4 vpMat = pMat * vMat;
	glUniformMatrix4fv(vpId, 1, GL_FALSE, &vpMat[0][0]);

	glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, plist.size() > ContainerSize ? ContainerSize : plist.size());
}
