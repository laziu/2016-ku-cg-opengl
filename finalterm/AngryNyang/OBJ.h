#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include "macro.h"

class OBJ
{
public:
	enum class type { obj, bin };

private:
	bool initialized;
public:
	std::vector<glm::vec4>	vertices;	// (x, y, z[, w = 1])
	std::vector<glm::vec3>	texCoords;	// (u, v[, w = 0]), range:[0, 1]
	std::vector<glm::vec3>	normals;	// (x, y, z), might not be unit vector
	
	OBJ(): initialized(false) {}
	OBJ(std::string filename, OBJ::type filetype = type::obj): OBJ() {
		switch ( filetype ) {
		case type::obj: Load  (filename); break;
		case type::bin: Reload(filename); break;
		}
	}

	bool operator!() const {
		return !initialized;
	}

	FORCED_MOVE_CONSTRUCTOR(OBJ) {
		std::swap(vertices , o.vertices );
		std::swap(texCoords, o.texCoords);
		std::swap(normals  , o.normals  );
		o.initialized = false;
	}

public:
	// load from obj file 
	void Load(std::string filename);
	// unload class data
	void Unload();

	// load from bin file
	void Reload(std::string binname);
	// save obj data to bin file
	bool Serialize(std::string binname);
};

