#version 330 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTexCoord;
layout(location = 2) in vec3 vNormal;

out vec3 fPosition;
out vec2 fTexCoord;
out vec3 fNormal;
out vec3 fCameraDir;
out vec3 fLightDir;

uniform mat4 MVP;
uniform mat4 V;
uniform mat4 M;
uniform vec3 LightPosition;

void main(){
	gl_Position =  MVP * vec4(vPosition, 1.0);

	fPosition =   (    M * vec4(vPosition, 1.0)).xyz;
	fNormal   =   (V * M * vec4(vNormal  , 0.0)).xyz;
	fCameraDir = -(V * M * vec4(vPosition, 1.0)).xyz;
	fLightDir  =  (V     * vec4(LightPosition, 1.0)).xyz + fCameraDir;
	fTexCoord = vTexCoord;
}
