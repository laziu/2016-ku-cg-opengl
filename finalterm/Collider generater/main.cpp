/// Collider Generator 
/// 
/// parse specified *.obj file to *.boxes file
///
/// only accepts blender's exported obj that
/// - objects as OBJ objects
/// - Apply Modifiers
///
/// then obj file should be
///   o ...
///   v x0 y0 z0           n:xyz
///   v x1 y1 z1       4:100 - 0:000
///   v x2 y2 z2         |       |      Floor
///   v x3 y3 z3       6:110 - 2:010
///   v x4 y4 z4
///   v x5 y5 z5       5:101 - 1:001
///   v x6 y6 z6         |       |      Ceil
///   v x7 y7 z7       7:111 - 3:011
///   
/// 
#pragma warning(disable:4996)
#include <cstdio>
#include <vector>
#include <array>
#include <glm/glm.hpp>

std::array<std::array<char const*, 2>, 11> input_name = {{
	{"resource/interior-colider-room.obj"        , "room"        },
	{"resource/interior-colider-table_center.obj", "table-center"},
	{"resource/interior-colider-table_corner.obj", "table-corner"},
	{"resource/interior-colider-table_tv.obj"    , "table-tv"    },
	{"resource/interior-colider-sofa_front.obj"  , "sofa-front"  },
	{"resource/interior-colider-sofa_side.obj"   , "sofa-side"   },
	{"resource/interior-colider-chair_1.obj"     , "chair-1"     },
	{"resource/interior-colider-chair_2.obj"     , "chair-2"     },
	{"resource/interior-colider-chair_3.obj"     , "chair-3"     },
	{"resource/interior-colider-chair_4.obj"     , "chair-4"     },
	{"resource/interior-colider-television.obj"  , "tv"          }
}};

char const* output_name = "output/interior-colider.boxes";

struct box
{
	glm::vec3 point[8];
};

std::vector<box> boxes;
char line[1000];
char key[1000];

int main() {
	
	FILE *out = fopen(output_name, "w");
	if ( !out ) {
		fprintf(stderr, "-- Error: %s could not be writed\n", output_name);
		return -1;
	}

	for ( auto const& iname : input_name ) {
		FILE *in = fopen(iname[0], "r");
		if ( !in ) {
			fprintf(stderr, "-- Error: %s is not exists\n", iname[0]);
			continue;
		}

		boxes.clear();

		int i = 0;
		box b;
		while ( fgets(line, 1000, in) ) {
			sscanf(line, "%s", key);
			if ( strcmp(key, "v") == 0 ) {
				glm::vec3 v;
				sscanf(line, "%s %f %f %f", key, &v.x, &v.y, &v.z);
				b.point[i++] = v;
				if ( i == 8 ) {
					boxes.push_back(b);
					i = 0;
				}
			}
		}

		fclose(in);

		fprintf(out, "o %s %d\n", iname[1], boxes.size());
		fprintf(out, "\n");
		for ( auto const& i : boxes ) {
			for ( int j = 0; j < 8; ++j ) {
				fprintf(out, "%f %f %f\n", i.point[j].x, i.point[j].y, i.point[j].z);
			}
			fprintf(out, "\n");
		}
	}

	fclose(out);

	return 0;
}