#include <cstdio>
#include <cmath>
#include <string>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "vec3.h"
#include "model.h"

namespace
{
	model m;
	// rotate angle
	float angle;
}

void problem3_init() {

	// solve each relation of edges
	for (int i = 0; i < 6; ++i) {
		for (int j = i + 1; j < 6; ++j) {
			// calculate dot value
			vec3 v1 = (m.edge(i)[1] - m.edge(i)[0]).normalized();
			vec3 v2 = (m.edge(j)[1] - m.edge(j)[0]).normalized();
			float dot_value = v1.dot(v2);
			
			// find relation 
			std::string relation = 
				(dot_value > 0.999f)?    "same direction"
				: (dot_value > 0.001f)?  "less than 90'"
				: (dot_value > -0.001f)? "orthogonal"
				: (dot_value > -0.999f)? "more than 90'"
				: "opposite direction";

			// print relation
			printf("  %6s + %6s: %3.0f' (%s)\n", name[i].c_str(), name[j].c_str(), 
				acosf(dot_value) * 180.0f / 3.141592f, relation.c_str());
		}
	}
}

void problem3_draw() {
	gluLookAt(4.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	// rotate model angle to view well
	angle += 0.001f;

	// draw edges 
	glPushMatrix();
		glRotatef(angle, 0.3f, 0.7f, 1.0f);

		glLineWidth(5.0f);
		glBegin(GL_LINES);
			for (int i = 0; i < 6; ++i) {
				glColor3fv(color[i]);
				glVertex3fv(m.edge(i)[0]);
				glColor3f(0.05f, 0.05f, 0.05f);
				glVertex3fv(m.edge(i)[1]);
			}
		glEnd();
	glPopMatrix();

	// draw sub objects for help understanding
	glPushMatrix();
		glTranslatef(0.0f, -1.5f, -1.5f);
		glRotatef(angle, 0.3f, 0.7f, 1.0f);

		glLineWidth(3.0f);
		glBegin(GL_LINES);
		for (int i = 0; i < 6; ++i) {
			vec3 pv = (m.edge(i)[1] - m.edge(i)[0]) / 3;
			glColor3fv(color[i]);
			glVertex3f(0.0f, 0.0f, 0.0f);
			glColor3f(0.05f, 0.05f, 0.05f);
			glVertex3fv(pv);
		}
		glEnd();
	glPopMatrix();
}
