#pragma once
#include <cmath>

namespace vec3index
{
	enum { x = 0, y = 1, z = 2 };
}

// float vector with 3 axis
struct vec3
{
	float p[3];
	float& operator[](int i);
	operator const float *() const;

	vec3& operator=(const vec3& v);
	vec3 operator+(const vec3& v) const;
	vec3 operator-(const vec3& v) const;
	vec3 operator-() const;
	float dot(const vec3& v) const;
	vec3 operator*(const vec3& v) const;
	float length() const;
	vec3 normalized() const;	
};
vec3 operator*(const vec3& a, float n);
vec3 operator/(const vec3& a, float n);
