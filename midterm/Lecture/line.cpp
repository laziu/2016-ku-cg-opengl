#include "line.h"

vec3 & line::operator[](int i) {
	return p[i];
}

line::operator const vec3*() const {
	return p;
}

vec3 line::midium() {
	using namespace lineindex;
	return (p[a] + p[b]) / 2.0f;
}
